<?php

namespace App\Controller;

use App\Entity\Comment;
use App\Entity\Product;
use App\Form\CommentType;
use App\Form\ProductType;
use App\Repository\ProductRepository;
use Cocur\Slugify\Slugify;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ProductController extends AbstractController
{

    /**
     * @Route("/", name="all")
     * @param ProductRepository $repository
     * @param PaginatorInterface $paginator
     * @param Request $request
     * @return Response
     */
    public function index(ProductRepository $repository, PaginatorInterface $paginator, Request $request) :Response
    {
        $products = $repository->findAll();
        $products = $paginator->paginate(
            $products,
            $request->query->getInt('page', 1),
            8
        );
        return $this->render('product/index.html.twig', ['products' => $products, 'active' => 'all']);
    }

    /**
     * @Route("/newProducts", name="newProducts")
     * @param ProductRepository $repository
     * @param PaginatorInterface $paginator
     * @param Request $request
     * @return Response
     */
    public function newProducts(ProductRepository $repository, PaginatorInterface $paginator, Request $request) :Response
    {
        $products = $repository->findBy(
            ['isNew' => true],
            ['addDate' => 'DESC']
        );
        $products = $paginator->paginate(
            $products,
            $request->query->getInt('page', 1),
            8
        );
        return $this->render('product/index.html.twig', ['products' => $products, 'active' => 'new']);
    }

    /**
     * @Route("/product/{slug}", name="product")
     * @param Product $product
     * @return Response
     */
    public function product(Product $product): Response
    {
        return $this->render('product/detail.html.twig', ['product' => $product]);
    }

    /**
     * @Route("/newComment/{slug}", name="new_comment")
     * @param Request $request
     * @param EntityManagerInterface $manager
     * @param Product $product
     * @return Response
     * @IsGranted("ROLE_USER")
     */
    public function newComment(Request $request, EntityManagerInterface $manager, Product $product): Response
    {
        $comment = new Comment();
        $form = $this->createForm(CommentType::class, $comment);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            $now = new\DateTime('now');
            $comment->setAddDate($now);
            $comment->setUser($this->getUser());
            $comment->setProduct($product);
            $manager->persist($comment);
            $manager->flush();
            $this->addFlash(
                'success',
                'Le commentaire a été correctement ajouté'
            );
            return $this->render('product/detail.html.twig', ['product' => $product]);
        }
        return $this->render('comment/add.html.twig', [
            'form' => $form->createView()
        ]);
    }


}
