<?php

namespace App\Controller;

use App\Entity\Category;
use App\Entity\Product;
use App\Form\CategoryType;
use App\Form\ProductType;
use App\Repository\CategoryRepository;
use App\Repository\ProductRepository;
use Cocur\Slugify\Slugify;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

class AdministrationController extends AbstractController
{

    /**
     * @Route("/administration", name="administration")
     * @param ProductRepository $repository
     * @param PaginatorInterface $paginator
     * @param Request $request
     * @IsGranted("ROLE_ADMIN")
     * @return Response
     */
    public function index(ProductRepository $repository, PaginatorInterface $paginator, Request $request) :Response
    {

        $products = $repository->findAll();
        $products = $paginator->paginate(
            $products,
            $request->query->getInt('page', 1),
            8
        );
        return $this->render('administration/product/index.html.twig', ['products' => $products, 'active' => 'prod']);
    }

    /**
     * @Route("/admin/product/{slug}", name="admin_product")
     * @param Product $product
     * @return Response
     */
    public function product(Product $product): Response
    {
        return $this->render('product/detail.html.twig', ['product' => $product]);
    }

    /**
     * @Route("/admin/delProduct/{slug}", name="del_product")
     * @param Product $product
     * @param EntityManagerInterface $manager
     * @return Response
     * @IsGranted("ROLE_ADMIN")
     */
    public function delProduct(Product $product, EntityManagerInterface $manager): Response {
        $this->delComments($manager, $product);
        $this->delProductDetails($manager, $product);
        $manager->remove($product);
        $manager->flush();
        $this->addFlash(
            'success',
            'Le produit a été correctement supprimé'
        );
        return $this->redirectToRoute('administration');
    }

    /**
     * @Route("/admin/newProduct", name="new_product")
     * @param Request $request
     * @param EntityManagerInterface $manager
     * @return Response
     * @IsGranted("ROLE_ADMIN")
     */
    public function newProduct(Request $request, EntityManagerInterface $manager): Response
    {
        $product = new Product;
        $form = $this->createForm(ProductType::class, $product);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            if(empty($product->getImage())) $product->setImage('default.png');
            $now = new\DateTime('now');
            if(empty($product->getAddDate())) $product->setAddDate($now);
            $slugify = new Slugify();
            $product->setSlug($slugify->slugify($product->getName()));
            $manager->persist($product);
            $manager->flush();
            $this->addFlash(
                'success',
                'Le produit a été correctement ajouté'
            );
            return $this->redirectToRoute('administration');
        }
        return $this->render('administration/product/add.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/admin/editProduct/{slug}", name="edit_product")
     * @param Request $request
     * @param EntityManagerInterface $manager
     * @param Product $product
     * @return Response
     * @IsGranted("ROLE_ADMIN")
     */
    public function editProduct(Request $request, EntityManagerInterface $manager, Product $product): Response
    {
        $form = $this->createForm(ProductType::class, $product);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            $slugify = new Slugify();
            $product->setSlug($slugify->slugify($product->getName()));
            $manager->flush();
            $this->addFlash(
                'success',
                'Le produit a été correctement modifié'
            );
            return $this->redirectToRoute('administration');
        }
        return $this->render('administration/product/edit.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/admin/category", name="category")
     * @param CategoryRepository $repository
     * @param PaginatorInterface $paginator
     * @param Request $request
     * @IsGranted("ROLE_ADMIN")
     * @return Response
     */
    public function category(CategoryRepository $repository, PaginatorInterface $paginator, Request $request) :Response
    {

        $categories = $repository->findAll();
        $categories = $paginator->paginate(
            $categories,
            $request->query->getInt('page', 1),
            8
        );
        return $this->render('administration/category/index.html.twig', ['categories' => $categories, 'active' => 'cat']);
    }

    /**
     * @Route("/admin/newCategory", name="new_category")
     * @param Request $request
     * @param EntityManagerInterface $manager
     * @return Response
     * @IsGranted("ROLE_ADMIN")
     */
    public function newCategory(Request $request, EntityManagerInterface $manager): Response
    {
        $category = new Category();
        $form = $this->createForm(CategoryType::class, $category);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            $manager->persist($category);
            $manager->flush();
            $this->addFlash(
                'success',
                'La categorie a été correctement ajouté'
            );
            return $this->redirectToRoute('category');
        }
        return $this->render('administration/category/add.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/admin/editCategory/{name}", name="edit_category")
     * @param Request $request
     * @param EntityManagerInterface $manager
     * @param Category $category
     * @return Response
     * @IsGranted("ROLE_ADMIN")
     */
    public function editCategory(Request $request, EntityManagerInterface $manager, Category $category): Response
    {
        $form = $this->createForm(CategoryType::class, $category);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            $manager->flush();
            $this->addFlash(
                'success',
                'La categorie a été correctement modifié'
            );
            return $this->redirectToRoute('category');
        }
        return $this->render('administration/category/edit.html.twig', [
            'form' => $form->createView()
        ]);
    }
    /**
     * @Route("/admin/delCategory/{name}", name="del_category")
     * @param Category $category
     * @param EntityManagerInterface $manager
     * @return Response
     * @IsGranted("ROLE_ADMIN")
     */
    public function delCategory(Category $category, EntityManagerInterface $manager): Response {
        $this->delCategoryProducts($manager, $category);
        $manager->remove($category);
        $manager->flush();
        $this->addFlash(
            'success',
            'La categorie a été correctement supprimé'
        );
        return $this->redirectToRoute('category');
    }

    private function delCategoryProducts(EntityManagerInterface $manager, Category $category)
    {
        $products = $category->getProducts();
        foreach ($products as $product)
        {
            $this->delProductDetails($manager, $product);
            $this->delComments($manager, $product);
            $manager->remove($product);

        }
        $manager->flush();
    }

    private function delComments(EntityManagerInterface $manager, Product $product)
    {
        $comments = $product->getComments();
        foreach ($comments as $comment){

            $manager->remove($comment);

        }
        $manager->flush();
    }

    private function delProductDetails(EntityManagerInterface $manager, Product $product)
    {
        $details = $product->getProductDetails();
        foreach ($details as $detail){

            $manager->remove($detail);

        }
        $manager->flush();
    }
}
