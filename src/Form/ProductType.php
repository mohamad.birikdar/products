<?php

namespace App\Form;

use App\Entity\Product;
use App\Entity\ProductDetails;
use Doctrine\DBAL\Types\DecimalType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichImageType;

class ProductType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('category', EntityType::class, [
                'label' => 'Sélectionnez une catégorie',
                'placeholder' => 'Sélectionnez...',
                'class' => 'App:Category',
                'choice_label' => 'name'
            ])
            ->add('name', TextType::class, [
                'label' => 'Nom de produit'
            ])
            ->add('addDate', DateType::class, [
                'label' => 'Date d\'ajoute',
                'data' => new \DateTime(),
                'format' => 'dd MM yyyy',
            ])
            ->add('price', NumberType::class, [
                'label' => 'Prix',
                ])
            ->add('promoPrice', NumberType::class, [
                'label' => 'Promo prix',
            ])
            ->add('isPromo', ChoiceType::class, [
                'label' => 'En Promotion?',
                'choices' => [
                    'Oui' => 1,
                    'Non' => 0
                ]
            ])
            ->add('isNew', ChoiceType::class, [
                'label' => 'Nouveau?',
                'choices' => [
                    'Oui' => 1,
                    'Non' => 0
                ]
            ])
            ->add('imageFile', VichImageType::class, [
                'label' => 'Image de produit',
                'required' => false,
                'attr' => ['placeholder' => 'image']
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Product::class,
        ]);
    }
}
