<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixtures extends Fixture
{
    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('fr_FR');
        for($i = 1; $i <= 30; $i++) {
            $user = new User();
            $user->setFirstName($faker->firstName);
            $user->setLastName($faker->lastName);
            $user->setUsername($user->getFirstName().$user->getLastName());
            $user->setEmail($user->getFirstName().'.'.$user->getLastName().'@gmail.com');
            $password = $this->encoder->encodePassword($user, 'password');
            $user->setPassword($password);
            $manager->persist($user);
        }
        $user = new User();
        $user->setFirstName('Admin');
        $user->setLastName('User');
        $user->setUsername('admin');
        $user->setEmail('admin@electro.be');
        $user->setRoles(['ROLE_ADMIN']);
        $password = $this->encoder->encodePassword($user, 'admin@123456');
        $user->setPassword($password);
        $manager->persist($user);

        $user = new User();
        $user->setFirstName('User');
        $user->setLastName('User');
        $user->setUsername('user');
        $user->setEmail('user@electro.be');
        $password = $this->encoder->encodePassword($user, 'user@123456');
        $user->setPassword($password);
        $manager->persist($user);

        $manager->flush();
    }
}
