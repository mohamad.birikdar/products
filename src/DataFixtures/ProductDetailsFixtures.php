<?php

namespace App\DataFixtures;

use App\Entity\Product;
use App\Entity\ProductDetails;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class ProductDetailsFixtures extends Fixture implements DependentFixtureInterface
{
    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {
        $products = $manager->getRepository(Product::class)->findAll();
        $faker = Factory::create('fr_FR');
        for($i = 1; $i <= 30; $i++) {
            $productDetails = new ProductDetails();
            $productDetails->setDetail($faker->words(3, true));
            $productDetails->setDetailValue($faker->randomNumber(1, true));
            $productDetails->setProduct($products[$faker->numberBetween(0, count($products) -1)]);
            $productDetails->setDescription($faker->words(15, true));
            $productDetails->setDetails($faker->words(15, true));

            $manager->persist($productDetails);
        }

        $manager->flush();
    }

    /**
     * This method must return an array of fixtures classes
     * on which the implementing class depends on
     *
     * @return class-string[]
     */
    public function getDependencies()
    {
        return [
           ProductFixtures::class
        ];
    }
}
