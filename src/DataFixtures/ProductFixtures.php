<?php

namespace App\DataFixtures;

use App\Entity\Category;
use App\Entity\Product;
use App\Entity\User;
use Cocur\Slugify\Slugify;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class ProductFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create(); // Instanciation de Faker
        $categories = $manager->getRepository(Category::class)->findAll(); // Récupération des objets categories
        $users = $manager->getRepository(User::class)->findAll(); // Récupération des objets users
        $slugify = new Slugify();

        for($i = 1; $i <= 30; $i++) {
            $price = $faker->numberBetween(50, 1000);
            $product = new Product();
            $product->setName($faker->words(3, true))
                 ->setIsPromo($faker->boolean(90))
                 ->setAddDate($faker->dateTimeBetween('-30 days', 'now'))
                 ->setImage($i.'.png')
                 ->setPrice($price)
                 ->setPromoPrice($faker->numberBetween(1, $price - 50))
                 ->setCategory($categories[$faker->numberBetween(0, count($categories) -1)])
                 ->setIsNew($faker->boolean(90))
                ->setSlug($slugify->slugify($product->getName()));;

            $manager->persist($product);
        }
        $manager->flush();
    }

    /**
     * This method must return an array of fixtures classes
     * on which the implementing class depends on
     *
     * @return class-string[]
     */
    public function getDependencies()
    {
        return [
            CategoryFixtures::class,
            UserFixtures::class
        ];
    }
}
