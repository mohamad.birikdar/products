<?php

namespace App\DataFixtures;

use App\Entity\Comment;
use App\Entity\Product;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
class CommentFixtures extends Fixture implements DependentFixtureInterface
{
    private $encoder;



    public function load(ObjectManager $manager)
    {

        $faker = Factory::create('fr_FR');
        $users = $manager->getRepository(User::class)->findAll();
        $products= $manager->getRepository(Product::class)->findAll();
        for($i = 1; $i <= 30; $i++) {
            $comment = new Comment();
            $comment->setUser($users[$faker->numberBetween(0, count($users) -1)]);
            $comment->setStars($faker->numberBetween(1, 5));
            $comment->setAddDate($faker->dateTimeBetween('-30 days', 'now'));
            $comment->setContent($faker->words(15, true));
            $comment->setProduct($products[$faker->numberBetween(0, count($products) -1)]);
            $manager->persist($comment);
        }

        $manager->flush();
    }

    /**
     * This method must return an array of fixtures classes
     * on which the implementing class depends on
     *
     * @return class-string[]
     */
    public function getDependencies()
    {
        return [
            UserFixtures::class,
            ProductFixtures::class
        ];
    }
}
